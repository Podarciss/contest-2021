<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shifumi</title>
    <link rel="shortcut icon" type="image/x-icon" href="src/img/rock.png" />
    <link rel="stylesheet" href="src/style.css">
  </head>
  <?php
    session_start();
    if (empty($_SESSION['nbTurns'])) {
      $_SESSION['nbTurns']=0;
      $_SESSION['nbVictory']=0;
      $_SESSION['nbDefaite']=0;
      $_SESSION['hour'] = date("G")+1;
      $_SESSION['minute'] = date("i");
      $_SESSION['nbTurns'] = 0;
    }
  ?>
  <body>
    <div class="bandeau">
      <?php
  	    echo "<p>Nombre de victoire: ".$_SESSION['nbVictory']."</p>";
        echo "<p>Nombre de defaite: ".$_SESSION['nbDefaite']."</p>";
        echo "<p>Heure de la première partie : ".$_SESSION['hour']."h".$_SESSION['minute']."</p>";
    	?>
    </div>
    <form action="result.php" method="post" class="form">
      <div class="choice">
        <img src="src/img/rock.png">
        <input type="submit" value="pierre" name="rock">
      </div class="picture">
      <div class="choice">
        <img src="src/img/paper.png">
        <input type="submit" value="feuille" name="paper">
      </div>
      <div class="choice">
        <img src="src/img/scissors.png">
        <input type="submit" value="ciseaux" name="scissors">
      </div>
    </form>
  </body>
</html>
