<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Shifumi - Resultat</title>
		<link rel="shortcut icon" type="image/x-icon" href="src/img/rock.png" />
		<link rel="stylesheet" href="src/style.css">
	</head>
	<body>
		<?php
			session_start();

			/*si aucune valeur n'est selectionnees*/
			if ((empty($_POST['rock']))&&(empty($_POST['paper']))&&(empty($_POST['scissors']))) {
				header('Location: index.php');
			}

			/*assignation du choix de l'utilisateur*/
			if (isset($_POST['rock'])) {
				$choiceUtil = 1;
			}else if(isset($_POST['paper'])) {
				$choiceUtil = 2;
			}else if(isset($_POST['scissors'])) {
				$choiceUtil = 3;
			}

			/*IA*/
			$choiceIA = rand(1,3);

			/*resultat du jeux*/
			if ($choiceIA == $choiceUtil) {
				$result = "egalite";
			}else if (($choiceIA == 1)&&($choiceUtil==3)){
				$result = "defaite";
				$_SESSION['nbDefaite']+=1;
			}else if (($choiceIA == 3)&&($choiceUtil==1)){
				$result = "victoire";
				$_SESSION['nbVictory']+=1;
			}else if ($choiceIA > $choiceUtil){
				$result = "defaite";
				$_SESSION['nbDefaite']+=1;
			}else if ($choiceUtil > $choiceIA){
				$result = "victoire";
				$_SESSION['nbVictory']+=1;
			}
    		$_SESSION['nbTurns'] += 1;
		?>

		<div id="content">
			<div class="bandeau">
				<?php
					echo "<p>Nombre de victoire: ".$_SESSION['nbVictory']."</p>";
					echo "<p>Nombre de defaite: ".$_SESSION['nbDefaite']."</p>";
					echo "<p>Heure de la première partie : ".$_SESSION['hour']."h".$_SESSION['minute']."</p>";
				?>
			</div>	
			<div class="choiceIA">
				<?php if ($choiceIA == 1){ ?>
					<img src="src/img/rock.png">
					<p>L'ordinateur à joué la pierre</p>
				<?php }else if ($choiceIA == 2){ ?>
					<img src="src/img/paper.png">
					<p>L'ordinateur à joué la feuille</p>
				<?php }else if ($choiceIA == 3){ ?>
					<img src="src/img/scissors.png">
					<p>L'ordinateur à joué les ciseaux</p>
				<?php } ?>
			</div>
			<div class="result">
				<?php if ($result == "victoire"){ ?>
					<p>Vous avez gagné!</p>
				<?php }else if ($result == "defaite"){ ?>
					<p>Vous avez perdu!</p>
				<?php }else if ($result == "egalite"){ ?>
					<p>Vous êtes à égalité!</p>
				<?php } ?>
			</div>
			<div id="inputsResult">
				<form class="form" action="index.php">
					<input type="submit" value="Rejouer">
				</form>
				<form class="form" action="exit.php" method="post">
					<input type="submit" value="Quitter" name="exit">
				</form>
			</div>
		</div>
	</body>
</html>