<?php
	$HOST = 'localhost';
	$DB_NAME = 'shifumi';
	$USER = 'root';
	$PASS = '';
	
	try{
		$db = new PDO("mysql:host=" . $HOST . ";dbname=" . $DB_NAME, $USER, $PASS);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e){
		echo $e;
	}

	try {
		global $db;
        $reussite = ($_SESSION['nbVictory']-$_SESSION['nbDefaite']);
        $tours = $_SESSION["nbTurns"];
        $ip = $_SERVER['REMOTE_ADDR'];
        $sth = $db->prepare("
            INSERT INTO utilisateurs(tauxReussite, nbTours, adresseIp)
            VALUES(:tauxReussite, :nbTours, :adresseIp)");
        $sth->bindParam(':tauxReussite',$reussite);
        $sth->bindParam(':nbTours',$tours);
        $sth->bindParam(':adresseIp',$ip);
        $sth->execute();
    }catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
?>
