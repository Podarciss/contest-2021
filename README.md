Le projet qui nous a été donné, nous demandait de reproduire le jeu du shifumi sous la forme de plusieurs pages visibles par l'utilisateur sous une certaine ergonomie,
en utilisant les languages php, html, et css. Le but était de nous évaluer, afin de mieux connaitre nos compétences en groupe. 

Nous avons décidé de réaliser 3 pages principals pour ce site, dont la première est la page d'accueil, mais aussi, c'est ici que nous faisons le choix (grâce à trois 
boutons différents) entre la pierre, la feuille et les ciseaux. Au-dessus de ces trois boutons, se trouvent 3 images représentants les différents choix.

Une fois un de ces boutons appuyé, l'utilisateur et redirections automatiquement vers notre deuxième page. À ce moment-là, une IA nommé Hal est générée automatiquement,
afin de faire un choix, lui-même, entre la pierre, le papier, et les ciseaux. L'IA est programmé pour réaliser une boucle qui avance à chaque fois
que l'utilisateur rejoue. Cette boucle est divisée en 5 parties, qui reboucle à chaque fois que le compteur atteint la 5ème partie. Pendant la première partie, 
l'IA prend un choix au hasard. Au second tour, il prend l'inverse de ce choix. Durant le troisième tour, il reprend le choix du premier tour. Et pendant le 4ème et 
le 5ème choix, il prend le choix qui n'a pas encore été prit. Ensuite, l'IA regarde qui a gagné, et retourne à l'utilisateur le choix qu'il avait pris
(entre la pierre, la feuille et les ciseaux), puis retourne le gagnant de cette partie. Aussi, est présente une bande en haut de la page, qui est aussi présent sur la 
page d'accueil. Sur cette bande, il est marqué trois choses: le compteur de victoire, le compteur de défaite, ainsi que l'heure de la première partie. Enfin, 
en bas de page se situent deux boutons. Le premier renvoie sur la page d'accueil (permettant de rejouer), et le second renvoi sur une page de déconnexion.

Sur cette page de déconnexion, il est juste écrit une phrase remerciant le joueur d'avoir joué. Cependant, un fichier permettant la connexion avec une base de données
(elle-même fournit (shifumi.sql)), permets de rentrer le nombre de parties jouées par l'utilisateur, l'adresse IP de celui-ci, la date et l'heure de déconnexion, ainsi
que le taux de victoire de l'utilisateur (nb de Victoire - nb de défaite). Suite à cela, la page de déconnexion remet tous les compteurs à 0, ainsi que l'heure de 
la première partie.

Le site a bien entendu été transmis à un hébergeur, et est disponible à cette adresse : 
https://shifumi40.herokuapp.com/