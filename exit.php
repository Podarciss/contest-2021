<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shifumi</title>
    <link rel="shortcut icon" type="image/x-icon" href="src/img/paper.png" />
    <link rel="stylesheet" href="src/style.css">
  </head>
  <body>
    <?php
      if (empty($_POST['exit'])) { 
        header('Location: index.php');
      }
    	session_start();
    	include('database.php');
      session_destroy();
    ?>
    <div id="end">
        <p>Merci d'avoir Joué au shifumi !!!</p>
    </div>
  </body>
</html>